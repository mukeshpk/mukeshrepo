﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dataaccesslayer;
using System.Data;
using commonmodels;
modified
namespace businesslayer
{
    public class blemployee
    {
        daemployee bnn = new daemployee();
        public DataSet program()
        {
            return bnn.emmpinfo();
        }
        public void insertEmployee(employeemodel mdl)
        {
            bnn.insertEmployee(mdl);
        }
        public void updateEmployee(employeemodel mdl)
        {
            bnn.updateEmployee(mdl);
        }
        public void deleteEmployee(int id)
        {
            bnn.deleteEmployee(id);
        }
    }
}
